/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kantinan.swingproject;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class friend implements Serializable{
    private String name;
    private int age;

    @Override
    public String toString() {
        return "friend{" + "name=" + name + ", age=" + age + ", gender=" + gender + ", desscription=" + desscription + '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDesscription() {
        return desscription;
    }

    public void setDesscription(String desscription) {
        this.desscription = desscription;
    }

    public friend(String name, int age, String gender, String desscription) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.desscription = desscription;
    }
    private  String gender;
    private  String desscription;
}
